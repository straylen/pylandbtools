%if 0%{?rhel} >= 8
%define py2_support 0
%define py3_support 1
%else
%if 0%{?rhel} >= 7
%define py2_support 1
%define py3_support 1
%endif
%endif


# Compatibility with RHEL. These macros have been added to EPEL but
# not yet to RHEL proper.
# https://bugzilla.redhat.com/show_bug.cgi?id=1307190
%{!?__python2: %global __python2 /usr/bin/python2}
%{!?__python3: %global __python3 /usr/bin/python3}
%{!?python2_sitelib: %global python2_sitelib %(%{__python2} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python3_sitelib: %global python3_sitelib %(%{__python3} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

Name:      landbtools
Version:   21.12.1
Release:   1%{?dist}
Summary:   Simplifies Python SOAP calls to CERN LanDB service
Vendor:    CERN
License:   MIT
Group:     System Environment/Base
URL:       https://gitlab.cern.ch/hw/pylandbtools
Source:    %{name}-%{version}.tgz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

BuildRequires:  perl-podlators
Conflicts:      CERN-CC-landbtools
%define __pathfix /usr/bin/pathfix.py -pni

%if 0%{py2_support}
Requires: python(abi) >= 2.6
Requires: python-suds
Requires: python-pycurl
BuildRequires:  python2-devel
BuildRequires:  python3-devel
BuildRequires:  python-setuptools
%define __python /usr/bin/python2
%define __python_shebangs_opts %{py2_shbang_opts}
%endif

%if 0%{py3_support}
Requires: python(abi) >= 3.6
%if 0%{?el7}
Requires: python36-suds
Requires: python36-pycurl
%else
%if 0%{?el8} || 0%{?el9}
Requires: python3.6dist(suds-jurko)
Requires: python3-pycurl
%endif
%endif

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

# If Python3 is available, redefine CLI shebang
%define __python /usr/bin/python3
%define __python_shebangs_opts %{py3_shbang_opts}
%endif


%description
Simplifies Python SOAP calls to CERN LanDB service,
provides a simple type factory and client service to
build generic queries to the WSDL-defined API.
Supports Kerberos5 environment for passwordless auth.
Provides landbGetInfo command for quick queries.

%prep
%setup -q

%build
pod2man -s -t man data/man/landbGetInfo.pod | gzip > data/man/landbGetInfo.1.gz
%if 0%{py2_support}
CFLAGS="%{optflags}" %{__python2} setup.py build
%endif

%if 0%{py3_support}
CFLAGS="%{optflags}" %{__python3} setup.py build
%endif


%install
%{__rm} -rf %{buildroot}

%if 0%{py2_support}
%{__python2} setup.py install --skip-build --root %{buildroot}
%endif

%if 0%{py3_support}
%{__python3} setup.py install --skip-build --root %{buildroot}
%endif

%{__mkdir} -p %{buildroot}%{_bindir}
%{__mkdir} -p %{buildroot}/%{_mandir}/man1

%{__install} -m 755 bin/landbGetInfo  %{buildroot}/usr/bin
%{__install} -m 644 data/man/landbGetInfo.1.gz %{buildroot}/%{_mandir}/man1/

%{__pathfix} "%{__python} %{__python_shebangs_opts}" %{buildroot}%{_bindir}/*
%{__rm} -f %{buildroot}%{_bindir}/*~

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/landbGetInfo
%{_mandir}/man1/landbGetInfo.1.gz

%if 0%{py2_support}
%{python2_sitelib}/*
%endif

%if 0%{py3_support}
%{python3_sitelib}/*
%endif

%doc AUTHORS COPYING

%changelog
* Fri Dec 3 2021 - Luca Gardi <luca.gardi@cern.ch> - 21.12.1-1
- deploy on CentOS 9 Stream

* Mon Apr 26 2021 <luca.gardi@cern.ch> - 21.4.5-1
- deploy on CentOS 8 Stream

* Wed Apr 14 2021 <luca.gardi@cern.ch> - 21.4.3-1
- Fixed a typo causing an exception in BytesIO declaration (Py3 only)

* Thu Dec 10 2020 <luca.gardi@cern.ch> - 20.12.2-1
- added c8s support
- fixed bug in py3 io import
- install for py2 and py3 on CC7

* Thu Dec 10 2020 <luca.gardi@cern.ch> - 20.12.2-1
- Fixed serial number attribute visibility
- minor linting

* Tue Nov 14 2017 <afiorot@cern.ch> - 2.0.1-1
- Fixed serial number attribute visibility
- minor linting

* Tue Nov 14 2017 <luca.gardi@cern.ch> - 2.0.0-4
- Fixed issue in custom cookie-path setter
- Wrote a proper README file for pylandb

* Tue Nov 14 2017 <luca.gardi@cern.ch> - 2.0.0-3
- this changelog is now correct

* Tue Nov 14 2017 <luca.gardi@cern.ch> - 2.0.0-2
- Now generated with correct dist in rpm

* Mon Nov 13 2017 <luca.gardi@cern.ch> - 2.0.0-1
- Rewrote everything in python
- Provides pylandb library

* Fri Jul 31 2015 <tfabio@cern.ch> - 1.2.7-2
- The OS name accepts spaces (Windows 2012 R2)

* Thu Jul 30 2015 <tfabio@cern.ch> - 1.2.7-1
- Added possibility to update Landb Operating System (name)

* Thu Jul 30 2015 <tfabio@cern.ch> - 1.2.6-1
- Added possibility to update Landb TAG and DESCRIPTION fields

* Wed Dec 10 2014 <aurelien@cern.ch> - 1.2.5-1
- Landb token support + other stuffs

* Wed Feb 26 2014 <aurelien@cern.ch> - 1.2.4-1
- LanDB token support

* Mon Jan 27 2014 <aurelien@cern.ch> - 1.2.3-2
- Hypervisor and VM checking

* Thu Feb 28 2013 <lefebure@cern.ch> - 1.2.3-1
- landbAddIPMI enhanced by Elisiano's patch (more checking)

* Wed Feb 13 2013 <uschwick@cern.ch> - 1.2.2-2
- resolve conflicts

* Tue Feb 12 2013 <lefebure@cern.ch> - 1.2.2-1
- Add landbAddIPMI

* Wed Jul 18 2012 <uschwick@cern.ch> - 1.2.1-1
- bug fix for -T flag

* Wed Jul 18 2012 <uschwick@cern.ch> - 1.2.0-1
- koji patches
- redo passwd treatment
- rename from CERN-CC-landbtools to landbtools

* Wed Jul 18 2012 <uschwick@cern.ch> - 1.1.5-1
- add landbRenameDevice
- koji patches
- rename from CERN-CC-landbtools to landbtools

* Tue Oct 11 2011 <uschwick@cern.ch> - 1.1.3-1
- allow blanks in cluster names

* Tue Aug 23 2011 <uschwick@cern.ch> - 1.1.2-1
- import updates to landbGetInfo from Elisiano
- jenkins and SLC6 build patches

* Wed May 18 2011 <uschwick@cern.ch> - 1.1.1-2
- add landbGetClusterInfo and landbMoveToPrimaryService

* Wed Nov 24 2010 <uschwick@cern.ch> - 1.1.0-2
- change logic in landbBindMAC to allow binding macs of
  interfaces on secondary services

* Fri Sep 17 2010 <uschwick@cern.ch> - 1.1.0-1
- change naming convention a bit (2 digits instead of 3)

* Fri Sep 10 2010 <uschwick@cern.ch> - 1.0.9-1
- fixings for 10GE machines upstairs

* Wed Sep 08 2010 <uschwick@cern.ch> - 1.0.8-1
- guess building from switch name instead of hardcoding it

* Wed Jun 23 2010 <uschwick@cern.ch> - 1.0.7-1
- add landbUpdateDevice

* Thu Jun 17 2010 <uschwick@cern.ch> - 1.0.6-1
- landbBindMAC now overwrites wrong bindings
- rename -G interface in landbMoveToSecondaryService
  following the naming conventions

* Tue Jun 1 2010 <uschwick@cern.ch> - 1.0.5-1
- add possibility to specify the first index of a VM in
  landbRegisterVM minor bug fix in landbGetInfo

* Tue Jun 1 2010 <uschwick@cern.ch> - 1.0.4-1
- add landbUnconfigure10GE
- reshuffle module handling
- add landbBindMAC

* Mon May 31 2010 <uschwick@cern.ch> - 1.0.2-1
- add landbRenameInterface
- make landbConfigure10GE more fail safe

* Mon May 10 2010 <uschwick@cern.ch> - 1.0.1-1
- make naming more consistent

* Mon May 10 2010 <uschwick@cern.ch> - 1.0.0-1
- repackage
- add tools for

* Thu Apr 22 2010 <uschwick@cern.ch> - 0.0.7-1
- add landbAddVM

* Fri Mar 26 2010 <uschwick@cern.ch> - 0.0.6-1
- increase max number of leases per hypervisor

* Thu Jan 28 2010 <uschwick@cern.ch> - 0.0.5-1
- add hypervisor name in the output of landbQueryVMs

* Mon Nov 16 2009 <uschwick@cern.ch> - 0.0.4-1
- bug fix ...

* Mon Nov 16 2009 <uschwick@cern.ch> - 0.0.3-1
- add landbGetInfo
- add landbMigrateVM

* Fri Nov 13 2009 <uschwick@cern.ch> - 0.0.2-1
- implement verbose mode for landbQuery

* Wed Nov 11 2009 <uschwick@cern.ch> - 0.0.1-2
- improved passwd passing in vmConfig.pm

* Mon Nov 9 2009 <uschwick@cern.ch> - 0.0.1-1
- Initial build.
