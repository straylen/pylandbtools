#!/usr/bin/env python
from setuptools import setup, find_packages
setup(
    name='landbtools',
    version='21.12.1',
    description="Simplifies Python SOAP calls to CERN LanDB service",
    author="Luca Gardi",
    long_description="Simplifies Python SOAP calls to CERN LanDB service, "
                     "provides a simple type factory and client service to "
                     "build generic queries to the WSDL-defined API."
                     "Supports Kerberos5 environment for passwordless auth."
                     "\n \n"
                     "Provides landbGetInfo command for quick queries.",
    author_email="luca.gardi@cern.ch",
    url="https://gitlab.cern.ch/hw/pylandbtools",
    maintainer='CERN IT Procurement',
    maintainer_email='procurement-team@cern.ch',
    license='MIT',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    scripts=['bin/landbGetInfo']
)
