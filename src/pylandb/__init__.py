import json
import logging
import os
import subprocess

import pycurl
from suds.client import Client
from suds.sax.element import Element
from suds.xsd.doctor import Import, ImportDoctor

try:
    from StringIO import StringIO as IOHandler
except ImportError:
    from io import BytesIO as IOHandler


class LanDB(object):
    """
        Simplifies SOAP calls to CERN LanDB service, provides a simple
        type factory and client service to build generic queries to the
        WSDL-defined API.

        Usage example:
            landb = LanDB()
            landb_service = landb.get_service()
            print landb_service.getDeviceInfo('VALIDHOSTNAME')
    """

    class GetSSOCookieError(Exception):
        """ Unable to retrieve cookie against CERN Single Sign-on service """
        pass

    TOKEN_URL = 'https://network.cern.ch/sc/soap/soap.fcgi/sso/6/token'
    API_URL = 'https://network.cern.ch/sc/soap/soap.fcgi?v=6&WSDL'
    API_SCHEMA_URL = 'http://schemas.xmlsoap.org/soap/encoding/'
    LOGGER_NAME = 'landb_handler'
    FOLDER_NAME = '.pylandb'
    COOKIE_NAME = 'webreq-sso-cookie.dat'
    CERN_AFS_HOME = '/afs/cern.ch/user/'
    CERN_AFS_PRIVATE = 'private'
    cookie_path = None
    token = None
    logging = None

    def __init__(self, debug=False, cookie_path=None):
        """ Initializes logging, authentication and SOAP client """

        if debug:
            loglevel = logging.DEBUG
        else:
            loglevel = logging.ERROR

        self.__set_logger(loglevel)
        self.__set_cookie_path(cookie_path)
        self.__set_cookie()
        self.__set_token()
        self.__set_client()
        self.logger.info('LanDB handler initialized')

    def __set_cookie_path(self, cookie_path):
        """ Checks and creates folder for temporary and safe token storage """
        self.logger.info('Setting cookie path')
        if not cookie_path:
            # Default cookie path
            home_path = os.path.expanduser('~/')
            if home_path.startswith(self.CERN_AFS_HOME):
                # Check if script is running in CERN AFS, in which case
                # use the ~/private folder to save the token file
                cookie_folder = os.path.join(home_path,
                                             self.CERN_AFS_PRIVATE,
                                             self.FOLDER_NAME)
            else:
                # Use user home
                cookie_folder = os.path.join(home_path, self.FOLDER_NAME)

            self.cookie_path = os.path.join(cookie_folder, self.COOKIE_NAME)
        else:
            # User specified folder
            self.cookie_path = os.path.join(cookie_path)

        # Check if folder exists
        if not os.path.exists(os.path.dirname(self.cookie_path)):
            self.logger.info("Creating folder %s"
                             % os.path.dirname(self.cookie_path))
            os.makedirs(os.path.dirname(self.cookie_path))

        self.logger.info('Cookie path set to %s' % self.cookie_path)

    def __set_logger(self, loglevel=None):
        """ Sets a logger and suppresses verbose logs from suds library """
        # Configuring logging options
        logging.getLogger('suds.client').setLevel(logging.ERROR)
        logging.getLogger('suds.transport').setLevel(logging.ERROR)
        logging.getLogger('suds.xsd.schema').setLevel(logging.ERROR)
        logging.getLogger('suds.transport.http').setLevel(logging.ERROR)

        self.logger = logging.getLogger(self.LOGGER_NAME)
        self.logger.setLevel(loglevel)

        if not self.logger.handlers:
            console_handler = logging.StreamHandler()
            console_handler.setLevel(logging.INFO)

            formatter = logging.Formatter(
                '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            console_handler.setFormatter(formatter)

            self.logger.addHandler(console_handler)
            self.logger.info('Logger initialized')

    def __set_cookie(self):
        """ Retrieves and writes authentication cookies """
        self.logger.info('Setting cookie')

        # Checking if path is writable
        try:
            with open(self.cookie_path, 'w+'):
                # Getting the cookie through cern-get-sso-cookie,
                # saved in ~/private/
                cmd_str = 'cern-get-sso-cookie -u "%s" '\
                          '-o "%s"' % (self.TOKEN_URL, self.cookie_path)
                cmd = subprocess.Popen(cmd_str, shell=True,
                                       executable='/bin/bash',
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
                cmd_rpl, dummy = cmd.communicate()
                if cmd_rpl:
                    raise self.GetSSOCookieError('Unexpected output for: %s'
                                                 % cmd_str)
        except IOError:
            raise self.GetSSOCookieError('Unable to write cookie file in %s '
                                         % self.cookie_path)

    def __set_token(self):
        """ Retrieves and sets authentication token, against LanDB SOAP
            APIs, using a stored cookie
        """
        self.logger.info('Setting token')
        output = IOHandler()
        curl_handler = pycurl.Curl()
        curl_handler.setopt(curl_handler.URL, str(self.TOKEN_URL))
        curl_handler.setopt(curl_handler.WRITEFUNCTION, output.write)
        curl_handler.setopt(curl_handler.FOLLOWLOCATION, True)
        curl_handler.setopt(curl_handler.COOKIEFILE, self.cookie_path)
        curl_handler.perform()
        curl_handler.close()

        response = json.loads(output.getvalue())
        self.token = response['token']

    def __set_client(self):
        """ Imports SOAP schema and sets the call client """
        self.logger.info('Setting client')
        # Creating the client
        schema = Import(self.API_SCHEMA_URL)
        doctor = ImportDoctor(schema)
        client = Client(self.API_URL, doctor=doctor, cache=None)

        # Authentication
        auth_token_element = Element('token').setText(self.token)
        auth_header_element = Element('Auth').insert(auth_token_element)
        client.set_options(soapheaders=auth_header_element)

        self.client = client

    def get_factory(self, types):
        """ Returns a types factory for LanDB WSDL definition """
        return self.client.factory.create(types)

    def get_service(self):
        """ Returns the SOAP client service for WSDL defined function calls """
        return self.client.service
